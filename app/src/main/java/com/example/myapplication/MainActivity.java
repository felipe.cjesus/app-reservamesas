package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private EditText editUsuario, editSenha;
    private Button btnLogin;

    HashMap<String, String> Usuarios = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Inclusão das chaves e valores do usuario utilizando hashmap
        Usuarios.put("Administrador", "Administrador");
        Usuarios.put("Adm", "Adm123");
        Usuarios.put("Administrador", "Que3B1eng4ElT0r0");
        Usuarios.put("Root", "pr0m1uscu0");

        editUsuario = findViewById(R.id.editUsuario);
        editSenha = findViewById(R.id.editSenha);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Verifica se campo usuario está vazio
                if(editUsuario.getText().toString().isEmpty()){
                    editUsuario.setError("Campo obrigatório!");
                }
                if(editSenha.getText().toString().isEmpty()){
                    editSenha.setError("Campo obrigatório!");
                }else{
                    if (Usuarios.containsKey(editUsuario.getText().toString())){
                        if(Usuarios.containsValue(editSenha.getText().toString())){
                            startActivity(new Intent(MainActivity.this, ReservaMesa.class));
                        }else {
                            editSenha.setError("Password Invalido!");
                        }
                    }else {
                        editUsuario.setError("Usuário Invalido!");
                    }
                }
            }
        });
    }
}
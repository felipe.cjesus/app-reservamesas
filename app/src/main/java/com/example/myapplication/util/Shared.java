package com.example.myapplication.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class Shared {

    public static final String KEY_USERNAME = "USERNAME";
    public static final String KEY_PASSWD = "PASSWD";
    public static  final String KEY_COR_MESA1 = "COR_MESA1";
    public static  final String KEY_COR_MESA2 = "COR_MESA2";
    public static  final String KEY_COR_MESA3 = "COR_MESA3";
    public static  final String KEY_COR_MESA4 = "COR_MESA4";
    public static  final String KEY_COR_MESA5 = "COR_MESA5";
    public static  final String KEY_COR_MESA6 = "COR_MESA6";
    public static  final String KEY_COR_MESA7 = "COR_MESA7";
    public static  final String KEY_COR_MESA8 = "COR_MESA8";
    public static  final String KEY_COR_MESA9 = "COR_MESA9";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;


    public Shared(final Context activity){
        preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        editor = preferences.edit();
    }

    public final void put(final String key, final Object value){

        if(value instanceof String){
            editor.putString(key, (String)value);
        }
        else if(value instanceof Boolean){
            editor.putBoolean(key, (Boolean)value);
        }
        else if(value instanceof Long){
            editor.putLong(key, (Long)value);
        }
        else if(value instanceof Integer){
            editor.putInt(key, (Integer)value);
        }
        else if(value instanceof Float){
            editor.putFloat(key, (Float)value);
        }else{
            //Toast.makeText(activity, "Tipo de dados invalido", Toast.LENGTH_LONG).show();
        }
        editor.apply();
    }

    public final String getString(final String key){
        return preferences.getString(key, "");
    }
    public final int getInt(final String key){
        return preferences.getInt(key, 0);
    }

}

package com.example.myapplication.dialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.example.myapplication.R;

import java.util.zip.Inflater;

public class InfoDialog {

    private Activity activity;
    private String titulo;
    private String mensagem;
    private TextView textTitulo;
    private TextView textMsg;
    private Button btnConfirmar;

    private AlertDialog dialog;

    public InfoDialog(final Activity activity, final String titulo, final String mensagem){
        this.activity = activity;
        this.titulo = titulo;
        this.mensagem = mensagem;
    }

    public void show(){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_info, null);
        builder.setView(view);
        builder.setCancelable(false);

        textTitulo = view.findViewById(R.id.textTitulo);
        textTitulo.setText(titulo);
        textMsg = view.findViewById(R.id.textMsg);
        textMsg.setText(mensagem);
        btnConfirmar = view.findViewById(R.id.btnConfirmar);
        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog = builder.create();
        dialog.show();
    }
}

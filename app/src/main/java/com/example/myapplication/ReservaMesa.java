package com.example.myapplication;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.dialog.InfoDialog;
import com.example.myapplication.util.Shared;

public class ReservaMesa extends AppCompatActivity {

    private Button btnReservar1, btnReservar2, btnReservar3, btnReservar4, btnReservar5, btnReservar6, btnReservar7, btnReservar8, btnReservar9, btnliberarMesa, btnReservarMesas, btnSalvar;
    private FrameLayout frmMesa1, frmMesa2, frmMesa3, frmMesa4, frmMesa5, frmMesa6, frmMesa7, frmMesa8, frmMesa9;
    private EditText numMesa;
    private Shared shared;
    private EditText editUsuario, editSenha;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);

        shared = new Shared(ReservaMesa.this);

        editUsuario = findViewById(R.id.editUsuario);
        editSenha = findViewById(R.id.editSenha);

        btnReservar1 = findViewById(R.id.btnReservar1);
        btnReservar2 = findViewById(R.id.btnReservar2);
        btnReservar3 = findViewById(R.id.btnReservar3);
        btnReservar4 = findViewById(R.id.btnReservar4);
        btnReservar5 = findViewById(R.id.btnReservar5);
        btnReservar6 = findViewById(R.id.btnReservar6);
        btnReservar7 = findViewById(R.id.btnReservar7);
        btnReservar8 = findViewById(R.id.btnReservar8);
        btnReservar9 = findViewById(R.id.btnReservar9);

        frmMesa1 = findViewById(R.id.frmMesa1);
        frmMesa2 = findViewById(R.id.frmMesa2);
        frmMesa3 = findViewById(R.id.frmMesa3);
        frmMesa4 = findViewById(R.id.frmMesa4);
        frmMesa5 = findViewById(R.id.frmMesa5);
        frmMesa6 = findViewById(R.id.frmMesa6);
        frmMesa7 = findViewById(R.id.frmMesa7);
        frmMesa8 = findViewById(R.id.frmMesa8);
        frmMesa9 = findViewById(R.id.frmMesa9);

        numMesa = findViewById(R.id.numMesa);

        // Ação do botão Salvar
        btnSalvar = findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                shared.put(Shared.KEY_USERNAME, editUsuario.getText().toString());
//                shared.put(Shared.KEY_PASSWD, editSenha.getText().toString());

                Drawable background = frmMesa1.getBackground();
                shared.put(Shared.KEY_COR_MESA1, ((ColorDrawable)background).getColor());

                Drawable background2 = frmMesa2.getBackground();
                shared.put(Shared.KEY_COR_MESA2, ((ColorDrawable)background2).getColor());

                Drawable background3 = frmMesa3.getBackground();
                shared.put(Shared.KEY_COR_MESA3, ((ColorDrawable)background3).getColor());

                Drawable background4 = frmMesa4.getBackground();
                shared.put(Shared.KEY_COR_MESA4, ((ColorDrawable)background4).getColor());

                Drawable background5 = frmMesa5.getBackground();
                shared.put(Shared.KEY_COR_MESA5, ((ColorDrawable)background5).getColor());

                Drawable background6 = frmMesa6.getBackground();
                shared.put(Shared.KEY_COR_MESA6, ((ColorDrawable)background6).getColor());

                Drawable background7 = frmMesa7.getBackground();
                shared.put(Shared.KEY_COR_MESA7, ((ColorDrawable)background7).getColor());

                Drawable background8 = frmMesa8.getBackground();
                shared.put(Shared.KEY_COR_MESA8, ((ColorDrawable)background8).getColor());

                Drawable background9 = frmMesa9.getBackground();
                shared.put(Shared.KEY_COR_MESA9, ((ColorDrawable)background9).getColor());

                InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Operação salva com sucesso!");
                dialog.show();
            }
        });

        // Seta situação das mesas quando clicado no botão Salvar Operação
        if(shared.getInt(Shared.KEY_COR_MESA1) != 0){
            frmMesa1.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA1));
        }
        if(shared.getInt(Shared.KEY_COR_MESA2) != 0){
            frmMesa2.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA2));
        }
        if(shared.getInt(Shared.KEY_COR_MESA3) != 0){
            frmMesa3.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA3));
        }
        if(shared.getInt(Shared.KEY_COR_MESA4) != 0){
            frmMesa4.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA4));
        }
        if(shared.getInt(Shared.KEY_COR_MESA5) != 0){
            frmMesa5.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA5));
        }
        if(shared.getInt(Shared.KEY_COR_MESA6) != 0){
            frmMesa6.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA6));
        }
        if(shared.getInt(Shared.KEY_COR_MESA7) != 0){
            frmMesa7.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA7));
        }
        if(shared.getInt(Shared.KEY_COR_MESA8) != 0){
            frmMesa8.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA8));
        }
        if(shared.getInt(Shared.KEY_COR_MESA9) != 0){
            frmMesa9.setBackgroundColor(shared.getInt(Shared.KEY_COR_MESA9));
        }


        // Seta usuario e senha quando clicado no botão Salvar Operação
//        editUsuario.setText(shared.getString(Shared.KEY_USERNAME));
//        editSenha.setText(shared.getString(Shared.KEY_PASSWD));


        // Ação do botão Reservar todas as Mesas
        btnReservarMesas = findViewById(R.id.btnReservarMesas);
        btnReservarMesas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Utilizado incremento para validar a quantidade de mesas usando o método isActivated para identificar mesa já reservada
                int i=0;
                if(!btnReservar1.isActivated()){
                    i++;
                }
                if(!btnReservar2.isActivated()){
                    i++;
                }
                if(!btnReservar3.isActivated()){
                    i++;
                }
                if(!btnReservar4.isActivated()){
                    i++;
                }
                if(!btnReservar5.isActivated()){
                    i++;
                }
                if(!btnReservar6.isActivated()){
                    i++;
                }
                if(!btnReservar7.isActivated()){
                    i++;
                }
                if(!btnReservar8.isActivated()){
                    i++;
                }
                if(!btnReservar9.isActivated()){
                    i++;
                }

                // Quantidade de mesas disponiveis e liberadas para alterar para reservadas e trocar as cores dos frames
                if(i>=9){
                    frmMesa1.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa2.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa3.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa4.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa5.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa6.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa7.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa8.setBackgroundColor(getResources().getColor(R.color.red));
                    frmMesa9.setBackgroundColor(getResources().getColor(R.color.red));

                    btnReservar1.setActivated(true);
                    btnReservar2.setActivated(true);
                    btnReservar3.setActivated(true);
                    btnReservar4.setActivated(true);
                    btnReservar5.setActivated(true);
                    btnReservar6.setActivated(true);
                    btnReservar7.setActivated(true);
                    btnReservar8.setActivated(true);
                    btnReservar9.setActivated(true);

                } else{
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Operação inválida, todas as mesas já possuem reserva");
                    dialog.show();
                }
            }
        });

        // Ação dos botões Reservar Mesa
        btnReservar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa1.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa1.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar1.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa2.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa2.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar2.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa3.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa3.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar3.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa4.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa4.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar4.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa5.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa5.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar5.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa6.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa6.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar6.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa7.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa7.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar7.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa8.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa8.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar8.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });
        btnReservar9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable background = frmMesa9.getBackground();
                if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.blue_dark)){
                    frmMesa9.setBackgroundColor(getResources().getColor(R.color.red));
                    btnReservar9.setActivated(true);
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa reservada com sucesso!");
                    dialog.show();
                }
            }
        });

        // Ação do botão Liberar Mesa
        btnliberarMesa = findViewById(R.id.btnliberarMesa);
        btnliberarMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(numMesa.getText().toString().equals("1")){
                    Drawable background = frmMesa1.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa1.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar1.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("2")){
                    Drawable background = frmMesa2.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa2.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar2.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("3")){
                    Drawable background = frmMesa3.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa3.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar3.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("4")){
                    Drawable background = frmMesa4.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa4.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar4.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("5")){
                    Drawable background = frmMesa5.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa5.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar5.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("6")){
                    Drawable background = frmMesa6.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa6.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar6.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("7")){
                    Drawable background = frmMesa7.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa7.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar7.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("8")){
                    Drawable background = frmMesa8.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa8.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar8.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }
                if(numMesa.getText().toString().equals("9")){
                    Drawable background = frmMesa9.getBackground();
                    if(((ColorDrawable)background).getColor() == getResources().getColor(R.color.red)){
                        frmMesa9.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnReservar9.setActivated(false);
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa liberada com sucesso!");
                        dialog.show();
                    } else {
                        InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não reservada. A mesa selecionada encontra-se\n" +
                                "habilitada para reserva");
                        dialog.show();
                    }
                }

                // Validação para mesas que não existem
                if(!numMesa.getText().toString().equals("")&&
                    !numMesa.getText().toString().equals("1")&&
                    !numMesa.getText().toString().equals("2")&&
                    !numMesa.getText().toString().equals("3")&&
                    !numMesa.getText().toString().equals("4")&&
                    !numMesa.getText().toString().equals("5")&&
                    !numMesa.getText().toString().equals("6")&&
                    !numMesa.getText().toString().equals("7")&&
                    !numMesa.getText().toString().equals("8")&&
                    !numMesa.getText().toString().equals("9")){
                    InfoDialog dialog = new InfoDialog(ReservaMesa.this, "Informação", "Mesa não existe! Informar mesa de 1 a 9!");
                    dialog.show();
                }
                if(numMesa.getText().toString().length() == 0){
                    numMesa.setError("Campo Obrigatório!");
                }

                //Limpa o edit text do numero da mesa
                numMesa.getText().clear();
            }
        });
    }
}
